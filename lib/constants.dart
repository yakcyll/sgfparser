/// Library containing project-specific constants.
library constants;

const int sgfFileFormatId = 4;
const int sgfGoGameMode = 1;
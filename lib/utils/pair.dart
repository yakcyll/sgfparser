/// Simple pair class implementation, used for representing points
/// and other two-element tuples.
class Pair<E, F> {
  late E first;
  late F second;

  Pair(this.first, this.second);
  Pair.fromList(List list) {
    first = list[0];
    second = list[1];
  }

  @override
  bool operator ==(covariant Pair other) {
    return first == other.first && second == other.second;
  }

  @override
  String toString() => '(' + first.toString() + ', ' + second.toString() + ')';

  @override
  int get hashCode => ImmutablePair(first, second).hashCode;
}

class ImmutablePair<E, F> {
  final E first;
  final F second;
  const ImmutablePair(this.first, this.second);

  @override
  bool operator ==(covariant ImmutablePair other) {
    return first == other.first && second == other.second;
  }

  @override
  String toString() => '(' + first.toString() + ', ' + second.toString() + ')';

  @override
  int get hashCode => Object.hash(first, second);
}

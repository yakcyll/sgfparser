import 'package:sgfparser/utils/pair.dart' show Pair;
import 'package:meta/meta.dart' show protected;
import 'package:flutter/foundation.dart';
import 'dart:math';

// Representation of a branch in a tree of positions.
// Contains a linear "sequence" of non-branching positions that may lead to
// a number of variations ("children"). sequence[0] represents the first
// position in the branch; sequence[-1] represents the last. Each child's
// sequence[0] represents the position after this.sequence[-1].
abstract class TicNodeTemplate<E> {
  // Node representing the previous sequence in a chain.
  late TicNodeTemplate? parent;
  // Represents a linear sequence of positions without branches.
  // It should only be valid for the root node of the position tree
  // to have an empty sequence. (TODO: assert this at runtime)
  late List<E> sequence;
  // Represents variations, with sequence[-1] being the position directly
  // preceeding each of the children's sequence[0] position.
  late List<TicNodeTemplate> children;
}

/// Base class for a tree iterator.
/// Allows traversing a sequence forward and back.
class TicIterator<Content, NodeType extends TicNodeTemplate>
    extends Iterator<Content> {
  @protected
  late NodeType? currentNode;
  @protected
  late int currentPositionIndex;
  @protected
  late List<int> pathBack, pathForward;
  static int tagCounter = 0;
  late int tag;

  TicIterator(NodeType? node, int positionIndex) {
    setPosition(node, positionIndex);
    tag = tagCounter;
    tagCounter += 1;
  }

  TicIterator.copy(TicIterator other) {
    currentNode = other.currentNode as NodeType;
    currentPositionIndex = other.currentPositionIndex;
    pathBack = List<int>.from(other.pathBack);
    pathForward = List<int>.from(other.pathForward);
    tag = tagCounter;
    tagCounter += 1;
  }

  /// Helper function constructing a path towards the end of the sequence
  /// starting from this iterator.
  List<int> _traverseCurrentVariationForward() {
    NodeType? tail = currentNode;
    int subsequencesTillEndCount = 0;
    if (tail == null) {
      return [];
    }

    while (tail!.children.isNotEmpty) {
      subsequencesTillEndCount += 1;
      tail = tail.children[0] as NodeType?;
    }

    return List<int>.filled(subsequencesTillEndCount, 0, growable: true);
  }

  /// Helper function constructing a path towards the root tree node.
  List<int> _traverseCurrentVariationBack() {
    List<int> pathBack = <int>[];
    NodeType? head = currentNode;
    if (head == null || head.parent == null) {
      return [];
    }
    while (head!.parent != null) {
      pathBack.insert(0, head.parent!.children.indexOf(head));
      head = head.parent as NodeType?;
    }

    return pathBack;
  }

  /// Function used to attach an iterator to a specific position in
  /// a position tree.
  void setPosition(NodeType? node, int positionIndex) {
    currentNode = node;
    currentPositionIndex = positionIndex;
    pathBack = _traverseCurrentVariationBack();
    pathForward = _traverseCurrentVariationForward();
  }

  /// Returns whether the iterator has a position forward on the sequence.
  bool hasNext() {
    assert(currentNode != null);
    return ((currentNode!.sequence.isNotEmpty &&
            currentPositionIndex < currentNode!.sequence.length - 1) ||
        currentNode!.children.isNotEmpty);
  }

  /// Function used to move the iterator forward on the sequence.
  /// Returns true if the update to the iterator was successful.
  @override
  bool moveNext() {
    assert(currentNode != null);

    if (!hasNext()) {
      return false;
    }

    if (currentPositionIndex < currentNode!.sequence.length - 1) {
      currentPositionIndex += 1;
    } else {
      assert(pathForward.isNotEmpty);
      assert(pathForward[0] < currentNode!.children.length);
      currentNode = currentNode!.children[pathForward[0]] as NodeType?;
      currentPositionIndex = 0;
      pathBack.add(pathForward.removeAt(0));
    }
    assert(currentPositionIndex < currentNode!.sequence.length);
    return true;
  }

  /// Helper function moving the iterator to the last position on the current sequence.
  bool moveLast() {
    assert(currentNode != null);

    if (!moveNext()) {
      assert(currentPositionIndex < currentNode!.sequence.length);
      return false;
    }

    while (moveNext()) {}
    assert(currentPositionIndex < currentNode!.sequence.length);
    return true;
  }

  /// Returns whether the iterator has a position back on the sequence.
  bool hasPrevious() {
    assert(currentNode != null);

    return (currentPositionIndex > 0 || currentNode!.parent != null);
  }

  /// Function used to move the iterator back on the sequence.
  /// Returns true if the update to the iterator was successful.
  bool movePrevious() {
    assert(currentNode != null);

    if (!hasPrevious()) {
      return false;
    }

    if (currentPositionIndex > 0) {
      currentPositionIndex -= 1;
    } else {
      currentNode = currentNode!.parent as NodeType?;
      currentPositionIndex = currentNode!.sequence.length - 1;
      pathForward.insert(0, pathBack.removeLast());
    }
    assert(currentPositionIndex >= 0);
    return true;
  }

  /// Helper function moving the iterator to the first position on the current sequence.
  bool moveFirst() {
    assert(currentNode != null);

    if (!movePrevious()) return false;
    while (movePrevious()) {}
    assert(currentPositionIndex >= 0);
    return true;
  }

  /// Helper function used to retrieve the position pointed to by this iterator.
  @override
  Content get current => currentNode?.sequence[currentPositionIndex];

  /// Helper function used to verify whether the iterator points to a valid position
  /// (i.e. there is a position at the position index of the sequence).
  bool isValid() =>
      currentNode != null &&
      currentPositionIndex < currentNode!.sequence.length;

  /// Helper function used to retrieve the identifiers of the position on a sequence.
  Pair<NodeType?, int> getCurrentPositionInTree() =>
      Pair(currentNode, currentPositionIndex);

  TicIterator operator +(other) {
    if (other is! int) {
      return this;
    }

    TicIterator newIter = TicIterator.copy(this);
    while (other > 0) {
      newIter.moveNext();
      other -= 1;
    }

    return newIter;
  }

  TicIterator operator -(other) {
    if (other is! int) {
      return this;
    }

    TicIterator newIter = TicIterator.copy(this);
    while (other > 0) {
      newIter.movePrevious();
      other -= 1;
    }

    return newIter;
  }

  /// Function used to obtain an iterator pointing to a position `other` moves
  /// forward on the sequence.
  @override
  bool operator ==(other) {
    if (!(other is TicIterator)) {
      return false;
    }

    TicIterator otherIter = other;
    return currentNode == otherIter.currentNode &&
        currentPositionIndex == otherIter.currentPositionIndex &&
        listEquals(pathBack, otherIter.pathBack) &&
        listEquals(pathForward, otherIter.pathForward);
  }

  /// Function used to verify if an iterator is higher in the tree than the other.
  bool operator <(other) {
    if (!(other is TicIterator)) {
      return false;
    }

    TicIterator otherIter = other;
    return (currentNode == otherIter.currentNode &&
            currentPositionIndex < otherIter.currentPositionIndex) ||
        (pathBack.length < otherIter.pathBack.length &&
            listEquals(List.from(pathBack).sublist(0, pathBack.length),
                List.from(otherIter.pathBack).sublist(0, pathBack.length)));
  }
}

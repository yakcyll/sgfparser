import 'package:sgfparser/goban/stone.dart' show StoneStatus, ModificationType;
import 'package:sgfparser/goban/boardTreeIterator.dart' show BoardTreeIterator;

import 'dart:math';
import 'package:flutter/foundation.dart';

List<StoneStatus> collectStoneChangesBetweenPositions(
    BoardTreeIterator from, BoardTreeIterator to) {
  // NOTE(yakcyll): Let's assume that from and to iterate over the same tree.
  // Currently I'm not exactly sure how to quickly verify this, other than
  // by keeping some metadata in each node (O(1)) / some nodes (O(n)).

  assert(from.isValid() && to.isValid());
  // debugPrint(
  //     "searching for stone changes {${from.moveNumber} ${from.currentPositionIndex} ${from.pathBack} ${from.pathForward}}, {${to.moveNumber} ${to.currentPositionIndex} ${to.pathBack} ${to.pathForward}}");

  List<StoneStatus> statusChanges = [];

  BoardTreeIterator traverser = BoardTreeIterator.copy(from);

  // First, check if the paths back are the same. If so, both are located in the sequence.
  // debugPrint("from back ${from.pathBack}, to back ${to.pathBack}");
  // debugPrint("from forward ${from.pathForward}, to forward ${to.pathForward}");
  if (listEquals(from.pathBack, to.pathBack)) {
    int positionDifference =
        from.currentPositionIndex - to.currentPositionIndex;

    /// debugPrint("pos diff $positionDifference");
    /// debugPrint(
    ///     "pos diff $positionDifference traverser index ${traverser.currentPositionIndex} to index ${to.currentPositionIndex}");
    if (positionDifference > 0) {
      // from is later in the sequence; push back until to is reached while collecting reversed stone statuses.
      while (traverser != to) {
        for (StoneStatus status in traverser.current.stonesChanged) {
          late ModificationType newType;
          switch (status.change) {
            case ModificationType.add:
              newType = ModificationType.remove;
              break;
            case ModificationType.remove:
              newType = ModificationType.add;
              break;
            case ModificationType.none:
              newType = ModificationType.none;
              break;
          }
          ;
          statusChanges.add(StoneStatus(status.color, status.coord, newType));
        }
        traverser.movePrevious();
      }
    } else if (positionDifference < 0) {
      // to is later in the sequence; push forward until from is reached while collecting stone statuses.
      while (traverser != to) {
        traverser.moveNext();
        statusChanges += traverser.current.stonesChanged;
      }
    }
    return statusChanges;
  }

  // If not, obtain the common prefix of branches between root and from and between root and to.
  List<int> commonBranches = [];
  for (int i = 0; i < min(from.pathBack.length, to.pathBack.length); ++i) {
    if (to.pathBack[i] == from.pathBack[i]) {
      commonBranches.add(from.pathBack[i]);
    } else {
      break;
    }
  }

  // debugPrint("common branches on path back ${commonBranches}");

  // Next, collect reversed stonesChanged from tree nodes while pushing the traverser back, until a common variation is reached.
  while (!listEquals(traverser.pathBack, commonBranches)) {
    for (StoneStatus status in traverser.current.stonesChanged) {
      late ModificationType newType;
      switch (status.change) {
        case ModificationType.add:
          newType = ModificationType.remove;
          break;
        case ModificationType.remove:
          newType = ModificationType.add;
          break;
        case ModificationType.none:
          newType = ModificationType.none;
          break;
      }
      statusChanges.add(StoneStatus(status.color, status.coord, newType));
    }
    traverser.movePrevious();
  }

  if (listEquals(traverser.pathBack, to.pathBack)) {
    // This means that to is strictly behind traverser; so simply move back.
    // debugPrint("to behind traverser after reaching a common subtree root?");
    while (to < traverser) {
      // debugPrint(
      //     "i'm stuck {${traverser.moveNumber} ${traverser.currentPositionIndex} ${traverser.pathBack} ${traverser.pathForward}}, {${to.moveNumber} ${to.currentPositionIndex} ${to.pathBack} ${to.pathForward}}");

      for (StoneStatus status in traverser.current.stonesChanged) {
        late ModificationType newType;
        switch (status.change) {
          case ModificationType.add:
            newType = ModificationType.remove;
            break;
          case ModificationType.remove:
            newType = ModificationType.add;
            break;
          case ModificationType.none:
            newType = ModificationType.none;
            break;
        }
        statusChanges.add(StoneStatus(status.color, status.coord, newType));
      }
      traverser.movePrevious();
    }

    return statusChanges;
  }

  // debugPrint(
  //     "before path forward adjustment ${traverser.pathBack} ${traverser.pathForward}}, {${to.pathBack} ${to.pathForward}}");

  // Adjust the path forward to lead towards the to.
  traverser.pathForward = List.from(to.pathBack);
  traverser.pathForward.removeRange(0, commonBranches.length);
  traverser.pathForward += to.pathForward;

  // debugPrint(
  //     "before a move forward {${traverser.moveNumber} ${traverser.currentPositionIndex} ${traverser.pathBack} ${traverser.pathForward}}, {${to.moveNumber} ${to.currentPositionIndex} ${to.pathBack} ${to.pathForward}}");

  // And move forward until to is reached, collecting status changes along the way.
  while (traverser != to) {
    // debugPrint(
    //     "i'm stuck again {${traverser.moveNumber} ${traverser.currentPositionIndex} ${traverser.pathBack} ${traverser.pathForward}}, {${to.moveNumber} ${to.currentPositionIndex} ${to.pathBack} ${to.pathForward}}");

    traverser.moveNext();
    statusChanges += traverser.current.stonesChanged;
  }

  // debugPrint("got $statusChanges");

  // debugPrint("from back ${from.pathBack}, to back ${to.pathBack}");
  // debugPrint("from forward ${from.pathForward}, to forward ${to.pathForward}");
  return statusChanges;
}

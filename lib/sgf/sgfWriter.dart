import 'package:sgfparser/sgf/node.dart';
import 'package:sgfparser/sgf/treeNode.dart';

class SGFWriter {
  String application = "sgfparser.dart:1";

  SGFWriter({String? application}) {
    if (application != null) this.application = application;
  }

  String writeProp(String propId, List<String> propValues) {
    String result = propId;
    for (String propValue in propValues) {
      result += '[' + propValue + ']';
    }
    return result;
  }

  String writeNode(SGFNode node) {
    String result = '';
    for (MapEntry property in node.properties.entries) {
      result += writeProp(property.key, property.value);
    }
    return result + ';';
  }

  String writeTreeNode(SGFTreeNode tree) {
    String result = '(;';
    for (SGFNode node in tree.sequence) {
      result += writeNode(node);
    }
    for (SGFTreeNode subTree in tree.children as List<SGFTreeNode>) {
      result += writeTreeNode(subTree);
    }
    result += ')';
    return result;
  }

  String writeSgf(SGFTreeNode tree) {
    // TODO: GC optimization

    tree.sequence[0].properties['AP'] = List.filled(1, application);
    tree.sequence[0].properties['ST'] =
        List.filled(1, '2'); // successor markup, no auto board markup
    return writeTreeNode(tree);
  }
}

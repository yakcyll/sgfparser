/// Class representing a descriptor containing SGF game metadata.
class GameMetadata {
  late String name;
  late String extraInfo;
  late String location;
  late DateTime gameDate;
  late String atEvent;
  late String round;
  late String source;
  late String user;

  GameMetadata() {
    name = '';
    extraInfo = '';
    location = '';
    gameDate = DateTime.now();
    atEvent = '';
    round = '';
    source = '';
    user = '';
  }

  GameMetadata.copy(GameMetadata other) {
    name = other.name;
    extraInfo = other.extraInfo;
    location = other.location;
    gameDate = other.gameDate;
    atEvent = other.atEvent;
    round = other.round;
    source = other.source;
    user = other.user;
  }
}

import 'package:sgfparser/goban/stone.dart' show StoneColor;

enum Reason { score, time, resignation, voided, unknown }

/// Class representing a descriptor of a result of a match.
class Result {
  late StoneColor winner;
  late Reason reason;
  late double score;

  Result() {
    winner = StoneColor.empty();
    reason = Reason.unknown;
    score = 0.0;
  }

  Result.copy(Result other) {
    winner = StoneColor.copy(other.winner);
    reason = other.reason;
    score = other.score;
  }

  static Reason translateReason(String str) {
    Map simpleReasons = {'R': Reason.resignation, 'T': Reason.time};
    if (simpleReasons.containsKey(str)) {
      return simpleReasons[str];
    }

    if (double.tryParse(str) != null) {
      return Reason.score;
    }

    return Reason.unknown;
  }
}

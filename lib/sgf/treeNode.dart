import 'package:sgfparser/sgf/node.dart' show SGFNode;
import 'package:sgfparser/sgf/treeNodeIterator.dart' show SGFTreeIterator;
import 'package:sgfparser/utils/ticIterator.dart' show TicNodeTemplate;
import 'dart:collection';

/// Class representing an SGF tree node.
/// Extends the basic project iterator with custom SGF specific constructors.
class SGFTreeNode extends TicNodeTemplate<SGFNode> with IterableMixin<SGFNode> {
  SGFTreeNode() {
    parent = null;
    sequence = <SGFNode>[];
    children = <SGFTreeNode>[];
  }

  SGFTreeNode.copy(SGFTreeNode other) {
    parent = null; // Cloning the parent would create another copy of this
    // node, so the caller has to assign the parent separately.

    sequence = List.generate(
        other.sequence.length, (index) => SGFNode.copy(other.sequence[index]));

    children = List<SGFTreeNode>.generate(
        other.children.length,
        (index) => SGFTreeNode.copy(other.children[index] as SGFTreeNode)
          ..parent = this);
  }

  factory SGFTreeNode.newGame() {
    var newGame = SGFTreeNode();
    newGame._prefillSequence();
    return newGame;
  }

  @override
  Iterator<SGFNode> get iterator => SGFTreeIterator(this, 0);

  void _prefillSequence() {}
}

import 'package:sgfparser/constants.dart' as constants;

/// Class representing an SGF file header metadata.
class FileHeader {
  String sourceApplication = '';
  String fileEncoding = '';
  late int fileFormat;
  late int gameMode;

  FileHeader() {
    fileFormat = constants.sgfFileFormatId;
    gameMode = constants.sgfGoGameMode;
  }
}

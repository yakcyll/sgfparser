/// A template class for SGF events. Used to facilitate exchange of SGF data 
/// between networked clients.
abstract class SGFEvent {
  bool handler(dynamic target);
}
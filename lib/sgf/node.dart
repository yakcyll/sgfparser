/// Class representing a descriptor describing an SGF node.
class SGFNode {
  late Map<String, List<String>> properties;

  SGFNode() {
    properties = <String, List<String>>{};
  }

  SGFNode.copy(SGFNode other) {
    properties = Map.from(other.properties);
  }
}

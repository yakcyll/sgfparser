import 'package:petitparser/petitparser.dart';

import 'package:sgfparser/sgf/treeNode.dart' show SGFTreeNode;
import 'package:sgfparser/sgf/node.dart' show SGFNode;

/// Class representing an SGF file parser. Implements a PEG SGF parser
/// emulating the eBNF specification found at https://www.red-bean.com/sgf/.
class SGFFileParser {
  late List<SGFTreeNode> games;
  late SGFTreeNode? currentGameTree;
  late SGFNode? currentNode;

  final collection = undefined();
  final gameTree = undefined();
  final openGameTree = undefined();
  final closeGameTree = undefined();
  final sequence = undefined();
  final node = undefined();
  final nodeSeparator = undefined();
  final property = undefined();
  final propertyIdentifier = undefined();
  final propertyValue = undefined();
  final cValueType = undefined();
  final compose = undefined();
  final value = undefined();
  final text = undefined();
  
  SGFFileParser(List<SGFTreeNode> gameList) {
    games = gameList;
    currentGameTree = null;
    currentNode = null;

    collection.set(gameTree.plus().trim());
    gameTree.set(openGameTree & sequence.trim() & gameTree.star() & closeGameTree);
    openGameTree.set(char('(').trim().map(addGame));
    closeGameTree.set(char(')').trim().map(closeGame));
    sequence.set(node.plus().trim());
    node.set(nodeSeparator.trim() & property.star().trim());
    nodeSeparator.set(char(';').flatten().trim().map(addNode));
    property.set((propertyIdentifier & propertyValue.plus().trim()).flatten().trim().map(addProperty));
    propertyIdentifier.set(uppercase().plus());
    propertyValue.set((pattern('\\').not() & pattern('[')).trim() & cValueType & (pattern('\\').not() & pattern(']')).trim());
    cValueType.set(compose | value);
    compose.set(value.trim() & char(':').trim() & value.trim());
    value.set(text.optional()); // Should be (real | number | text | dbl | color | point | move | stone) and include simpleText, 
                                // but it's petitparser - it's PEG, it's greedy, those types are not necessary anyway.
    //text.set((whitespace() | word() | anyOf('.,'\'?!;:#\$%&()[*+-/<>=@^_{}|~') | char('\\') & char(']') | char('\\')).star());
    text.set((whitespace() | anyCharExcept(']\\') | (char('\\') & char(']')) | char('\\')).star());
    compose.set(value & char(':') & value);
  }

  /// Helper function used to construct an SGF tree during parsing;
  /// creates a new game tree and adds it to an existing one as a child
  /// or to the game list.
  dynamic addGame(String str) {
    SGFTreeNode newGameTree = SGFTreeNode();

    // print('adding game.');
    if (currentGameTree == null) {
      newGameTree.parent = null;
      games.add(newGameTree);
    }
    else {
      newGameTree.parent = currentGameTree;
      currentGameTree!.children.add(newGameTree);
    }

    currentGameTree = newGameTree;
  }

  /// Helper function used to construct an SGF tree during parsing;
  /// finishes parsing of a game tree.
  dynamic closeGame(String str) { 
    assert(currentGameTree != null);

    // print('closing game.');
    if (currentGameTree!.parent != null) {
      currentGameTree = currentGameTree!.parent as SGFTreeNode;
    } else {
      currentGameTree = null;
    }

    currentNode = null;
  }

  /// Helper function used to construct an SGF tree during parsing;
  /// creates a new SGF node and adds it to the current tree node's sequence.
  dynamic addNode(String str) {
    assert(currentGameTree != null);

    // print('adding node for string ' + str);
    currentNode = SGFNode();
    currentGameTree!.sequence.add(currentNode!);
  }

  /// Helper function used to construct an SGF tree during parsing;
  /// adds a property to the currently constructed SGF tree.
  dynamic addProperty(String properties) { 
    assert(currentNode != null);

    // print('adding properties: ${properties}.');
    String propertyIdentifier = properties.split('[')[0];
    List<String> propertyValues = [];
    int indexInString = properties.indexOf('[');
    int closerInString = indexInString;

    while (indexInString != -1) {
      closerInString = properties.indexOf(']', closerInString);
      if (closerInString != -1) {
        if (properties[closerInString - 1] != '\\') { // it means this is an actual closing bracket
          propertyValues.add(properties.substring(indexInString+1, closerInString));
          indexInString = properties.indexOf('[', closerInString);
          closerInString = indexInString;
        }
        else {
          closerInString += 1;
        }
      }
      else break; // TODO: unclosed properties are no-no; exception?
    }

    if (!currentNode!.properties.containsKey(propertyIdentifier)) {
      currentNode!.properties[propertyIdentifier] = propertyValues;
    } else {
      currentNode!.properties[propertyIdentifier]!.addAll(propertyValues);
    }
  }

  /// Function parsing a supplied SGF string and constructing an SGF tree 
  /// representation.
  void parse(String sgf) {
    final parser = collection.end();
    // print(parser.accept(sgf).toString());
    dynamic result = parser.parse(sgf);
    if (result is Failure) {
      // TODO: Handle this gracefully.
      throw Exception('Parse error: ${result.message} '
                      'at position ${result.position}, '
                      'instead got \'${sgf[result.position]}\'.');
    }
  }
}

Parser<String> anyCharExcept(String except,
    [String message = 'letter or digit expected']) {
  return CharacterParser(AnyCharExceptPredicate(except.codeUnits), message)
      .plus()
      .map((list) => list.join());
}

class AnyCharExceptPredicate implements CharacterPredicate {
  final List<int> exceptCodeUnits;
  AnyCharExceptPredicate(this.exceptCodeUnits);
  static final _ws = WhitespaceCharPredicate();

  @override
  bool test(int value) => !_ws.test(value) && !exceptCodeUnits.contains(value);

  @override
  bool isEqualTo(CharacterPredicate other) {
    return (other is AnyCharExceptPredicate) && identical(this, other);
  }
}
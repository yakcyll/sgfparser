import 'package:sgfparser/utils/pair.dart' show Pair;

import 'package:sgfparser/goban/stone.dart' show StoneColor;
import 'package:sgfparser/sgf/result.dart' show Result;

/// Class representing a descriptor containing SGF match metadata.
class MatchInfo {
  late String blackName;
  late String blackRank;
  late String blackTeam;

  late String whiteName;
  late String whiteRank;
  late String whiteTeam;

  late Pair<int, int> size;

  late String ruleset;
  late Result result;
  late double komi;
  late double timePerPlayer;
  late int handicap;

  MatchInfo() {
    blackName = '';
    blackRank = '';
    blackTeam = '';
    whiteName = '';
    whiteRank = '';
    whiteTeam = '';
    size = Pair(19, 19);
    ruleset = '';
    result = Result();
    komi = 0.0;
    timePerPlayer = 0.0;
    handicap = 0;
  }

  MatchInfo.copy(MatchInfo other) {
    blackName = other.blackName;
    blackRank = other.blackRank;
    blackTeam = other.blackTeam;
    whiteName = other.whiteName;
    whiteRank = other.whiteRank;
    whiteTeam = other.whiteTeam;
    size = other.size;
    ruleset = other.ruleset;
    result = Result.copy(other.result);
    komi = other.komi;
    timePerPlayer = other.timePerPlayer;
    handicap = other.handicap;
  }
}

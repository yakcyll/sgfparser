import 'package:sgfparser/sgf/fileHeader.dart' show FileHeader;
import 'package:sgfparser/sgf/gameMetadata.dart' show GameMetadata;
import 'package:sgfparser/sgf/matchInfo.dart' show MatchInfo;

/// A descriptor of SGF metadata, including the file header
/// and various game information.
class SgfTreeDescription {
  late FileHeader fileHeader;
  late GameMetadata gameMetadata;
  late MatchInfo matchInfo;

  SgfTreeDescription() {
    fileHeader = FileHeader();
    gameMetadata = GameMetadata();
    matchInfo = MatchInfo();
  }
}

import 'package:sgfparser/sgf/node.dart' show SGFNode;
import 'package:sgfparser/sgf/treeNode.dart' show SGFTreeNode;
import 'package:sgfparser/utils/ticIterator.dart' show TicIterator;

/// Class representing an SGF tree iterator.
class SGFTreeIterator extends TicIterator<SGFNode, SGFTreeNode> {
  SGFTreeIterator(SGFTreeNode currentNode, int positionIndex) : super(currentNode, positionIndex);
  SGFTreeIterator.copy(TicIterator other) : super.copy(other);

  @override
  SGFTreeIterator operator +(other) => SGFTreeIterator.copy(super+(other));

  @override
  SGFTreeIterator operator -(other) => SGFTreeIterator.copy(super-(other));
}
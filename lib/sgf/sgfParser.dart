import 'dart:developer' as dev;
import 'dart:math' show min, max;
import 'package:flutter/foundation.dart';

import 'package:petitparser/petitparser.dart';
import 'package:validators/sanitizers.dart';

import 'package:sgfparser/constants.dart' as constants;

import 'package:sgfparser/goban/rulesets/commonRules.dart'
    show CommonTwoPlayerRules;
import 'package:sgfparser/goban/stone.dart'
    show StoneColor, StoneStatus, ModificationType;
import 'package:sgfparser/goban/gameState.dart' show GameState;
import 'package:sgfparser/goban/match.dart' show Match;
import 'package:sgfparser/goban/board.dart' show Board;
import 'package:sgfparser/goban/boardTreeNode.dart' show BoardTreeNode;
import 'package:sgfparser/goban/boardTreeIterator.dart' show BoardTreeIterator;
import 'package:sgfparser/goban/boardState.dart'
    show BoardState, Evaluation, PositionEvaluationType, MoveEvaluationType;

import 'package:sgfparser/sgf/fileParser.dart'
    show SGFFileParser, anyCharExcept;
import 'package:sgfparser/sgf/treeNode.dart' show SGFTreeNode;
import 'package:sgfparser/sgf/node.dart' show SGFNode;
import 'package:sgfparser/sgf/result.dart' as sgfresult;
import 'package:sgfparser/sgf/sgfTreeDescription.dart' show SgfTreeDescription;

import 'package:sgfparser/utils/pair.dart' show Pair;

typedef NodeParserFunc = void Function(String propId, List<String> properties,
    {CommonTwoPlayerRules ruleset, BoardTreeIterator? position});

/// Class representing a complete SGF parser.
/// Primary constructor of SGF and board trees, encapsulates a file parser,
/// SGF tree node storage and a set of parsers for properties defined by
/// the SGF file format specification (FF[4]).
class SGFParser {
  late String fileBuffer;
  late SGFFileParser fileParser;
  late List<SGFTreeNode> games;
  late SgfTreeDescription sgfTreeDescription;

  late Map<String, NodeParserFunc> nodeParsers;

  SGFParser() {
    initParser();
  }
  SGFParser.fromFile(String fb) {
    initParser();
    fileBuffer = fb;
  }

  void initParser() {
    fileBuffer = '';
    games = <SGFTreeNode>[];
    fileParser = SGFFileParser(games);
    sgfTreeDescription = SgfTreeDescription();

    nodeParsers = <String, NodeParserFunc>{};

    _addNodeParsers();
  }

  /// Invokes the file parser to construct the list of SGF tree nodes.
  void parseBuffer({String? fb}) {
    if (fb != null) {
      fileBuffer = fb;
    }

    fileParser.parse(fileBuffer);
  }

  /// Helper function traversing the SGF trees constructed by the file parser
  /// and verifying correctness of the SGF file metadata.
  bool parseTree() {
    /* Technically `games` can be a tree of trees; should traverse all? */
    traverseTree(games[0]);

    /* Perform validation. */
    if (sgfTreeDescription.fileHeader.fileFormat > constants.sgfFileFormatId ||
        sgfTreeDescription.fileHeader.gameMode != constants.sgfGoGameMode) {
      return false;
    }

    return true;
  }

  /// Recursively traverses an SGF tree, parsing all nodes in the node before
  /// descending down.
  void traverseTree(SGFTreeNode sgfTree) {
    for (SGFNode node in sgfTree.sequence) {
      parseNode(node);
    }

    for (SGFTreeNode tree in sgfTree.children as List<SGFTreeNode>) {
      traverseTree(tree);
    }
  }

  /// Parses properties of a node.
  /// Can be supplied with a ruleset and position to perform manipulation
  /// of a board tree on the fly.
  void parseNode(SGFNode node, {GameState? game, BoardTreeIterator? position}) {
    if (position != null) {
      if (position.isValid()) {
        position.insertVariation(BoardState.copy(position.current));
        position.moveNext();
      } else {
        game?.positions.addPosition(BoardState.fromBoard(Board(
            sgfTreeDescription.matchInfo.size.first,
            sgfTreeDescription.matchInfo.size.second)));
      }
    }

    node.properties.forEach((key, value) {
      if (position != null) {
        assert(position.isValid());
      }

      if (nodeParsers.containsKey(key)) {
        nodeParsers[key]!(key, value,
            ruleset: game!.ruleset, position: position);
      } else {
        debugPrint("unknown key ${key} when parsing the SGF file");
        // dev.debugger();
      }
    });
  }

  /// Helper function to create a complete game state from an SGF string.
  /// Returns a new game with the board tree representing the match from
  /// the SGF file.
  void feedSgfToGame(Match match, String sgf) {
    GameState game = match.gameState;
    fileBuffer = sgf;
    parseBuffer();
    SGFTreeNode sgfTree = games[0];

    // Parse the root node to obtain the game metadata.
    parseNode(sgfTree.sequence[0], game: game);

    // Remove the default game position inserted by the GameState constructor.
    game.positions.sequence.removeAt(0);

    _traverseSgfTreeWithGameState(sgfTree, game, game.currentPosition);

    match.metadata = sgfTreeDescription.gameMetadata;
    match.matchInfo = sgfTreeDescription.matchInfo;
  }

  /// Helper function used to construct a board tree during SGF tree traversal.
  /// For each SGF tree child, it creates a new board tree node and temporarily
  /// adds an empty board or the last board from the parent board tree to its
  /// sequence. This board tree node is then supplied to node property parsers
  /// to be filled with moves and their metadata.
  void _traverseSgfTreeWithGameState(
      SGFTreeNode sgfTree, GameState game, BoardTreeIterator position) {
    for (SGFNode node in sgfTree.sequence) {
      parseNode(node, game: game, position: position);
      position.moveLast();
      assert(position.isValid());
    }

    /// For each child in the SGF tree, create a new board tree node and add
    for (SGFTreeNode tree in sgfTree.children as List<SGFTreeNode>) {
      BoardTreeNode? currentBoardTree =
          position.getCurrentPositionInTree().first;
      BoardTreeNode newBoardTree = BoardTreeNode();
      BoardTreeIterator? previousPosition = BoardTreeIterator.copy(position);

      currentBoardTree?.children.add(newBoardTree);

      if (previousPosition != null) {
        // This copy of a position will be removed once a tree node is fully constructed
        // to prevent duplicate positions. It is added because node processing expects
        // a position to exist in a sequence.
        // TODO: Verify that this approach works when a variation tree is created where
        // TODO: the corresponding SGF tree node has either a) no properties, b) only
        // TODO: only properties that modify a position in-place.
        newBoardTree.sequence.add(BoardState.copy(previousPosition.current));
      } else {
        // TODO: make sure game metadata is read before a node with moves/placements is
        // TODO: encountered (otherwise a position will be created with invalid dimensions).
        newBoardTree.addPosition(BoardState.fromBoard(Board(
            sgfTreeDescription.matchInfo.size.first,
            sgfTreeDescription.matchInfo.size.second)));
      }

      _traverseSgfTreeWithGameState(
          tree, game, BoardTreeIterator(newBoardTree, 0));

      if (newBoardTree.sequence.isNotEmpty) {
        newBoardTree.sequence.removeAt(0);
      }

      newBoardTree.parent = currentBoardTree;
    }
  }

  String _convertToSimpleText(String str) {
    return escape(str.replaceAll(RegExp(r'\n|\t|\v|\\ '), ' '));
  }

  /// Helper function constructing a node property parser map used when
  /// processing SGF nodes.
  void _addNodeParsers() {
    // file metadata properties
    nodeParsers['AP'] = _sourceApplicationParser;
    nodeParsers['CA'] = _fileEncodingParser;
    nodeParsers['GM'] = _gameModeParser;
    nodeParsers['FF'] = _fileFormatParser;
    nodeParsers['ST'] = _variationModeParser;

    // game info properties
    nodeParsers['DT'] = _gameDateParser;
    nodeParsers['EV'] = _atEventParser;
    nodeParsers['GN'] = _gameNameParser;
    nodeParsers['GC'] = _extraInfoParser;
    nodeParsers['PC'] = _gameLocationParser;
    nodeParsers['US'] = _userParser;
    nodeParsers['RO'] = _roundParser;
    nodeParsers['SO'] = _gameSourceParser;
    nodeParsers['PB'] = _blackNameParser;
    nodeParsers['BR'] = _blackRankParser;
    nodeParsers['BT'] = _blackTeamParser;
    nodeParsers['PW'] = _whiteNameParser;
    nodeParsers['WR'] = _whiteRankParser;
    nodeParsers['WT'] = _whiteTeamParser;
    nodeParsers['SZ'] = _boardSizeParser;
    nodeParsers['RU'] = _rulesetParser;
    nodeParsers['RE'] = _gameResultParser;
    nodeParsers['KM'] = _komiParser;
    nodeParsers['TM'] = _timePerPlayerParser;
    nodeParsers['OT'] = _toBeImplementedParser;
    nodeParsers['HA'] = _handicapParser;

    // move properties
    nodeParsers['B'] = nodeParsers['W'] = _moveParser;
    nodeParsers['BL'] = nodeParsers['WL'] = _timeLeftParser;
    nodeParsers['OB'] = nodeParsers['OW'] = _toBeImplementedParser;

    // move annotation properties
    nodeParsers['BM'] = _badMoveParser;
    nodeParsers['DO'] = _doubtfulMoveParser;
    nodeParsers['IT'] = _interestingMoveParser;
    nodeParsers['TE'] = _tesujiParser;

    // setup properties
    nodeParsers['AB'] = nodeParsers['AW'] = _moveSetupParser;
    nodeParsers['AE'] = _moveClearParser;
    nodeParsers['PL'] = _playerToMoveParser;

    // node annotation properties
    nodeParsers['C'] = _commentParser;
    nodeParsers['N'] = _nodeNameParser;
    nodeParsers['DM'] = _evenEvaluationParser;
    nodeParsers['GB'] = _blackBetterEvaluationParser;
    nodeParsers['GW'] = _whiteBetterEvaluationParser;
    nodeParsers['V'] = _nodeValueParser;
    nodeParsers['HO'] = _hotspotParser;
    nodeParsers['UC'] = _unclearEvaluationParser;

    // markup properties
    nodeParsers['AR'] = _arrowParser;
    nodeParsers['CR'] = _circleParser;
    nodeParsers['DD'] = _toBeImplementedParser;
    nodeParsers['LB'] = _labelParser;
    nodeParsers['LN'] = _lineParser;
    nodeParsers['MA'] = _crossParser;
    nodeParsers['SL'] = _toBeImplementedParser;
    nodeParsers['SQ'] = _squareParser;
    nodeParsers['TR'] = _triangleParser;
    nodeParsers['VW'] = _toBeImplementedParser;

    nodeParsers['TB'] = _toBeImplementedParser;
    nodeParsers['TW'] = _toBeImplementedParser;
  }

  int _getLetterIndex(String letter) {
    // + 26 is based on FF[4]'s ability to specify board sizes up to 52 (z = 26, A = 27).
    return letter.codeUnitAt(0) >= 97
        ? letter.codeUnitAt(0) - 97
        : letter.codeUnitAt(0) - 65 + 26;
  }

  /// Returns a coordinate for a move property ('B[xy]' or 'W[xy]').
  /// If the property is malformed, the coordinate will point outside of any board;
  /// a special case when the property is an empty string produces a `null`, which
  /// should be handled as a pass.
  List<Pair<int, int>> _getCoordinatesFromProperty(String property) {
    List<Pair<int, int>> coords = [];

    if ((pattern('[a-zA-Z]') &
            pattern('[a-zA-Z]') &
            char(':') &
            pattern('[a-zA-Z]') &
            pattern('[a-zA-Z]'))
        .accept(property)) {
      int firstCoord = _getLetterIndex(property[0]);
      int secondCoord = _getLetterIndex(property[1]);
      int thirdCoord = _getLetterIndex(property[3]);
      int fourthCoord = _getLetterIndex(property[4]);

      for (int i = min(firstCoord, thirdCoord);
          i <= max(firstCoord, thirdCoord);
          ++i) {
        for (int j = min(secondCoord, fourthCoord);
            j <= max(secondCoord, fourthCoord);
            ++j) {
          coords.add(Pair<int, int>(i, j));
        }
      }
    } else if ((pattern('[a-zA-Z]') & pattern('[a-zA-Z]')).accept(property)) {
      int firstCoord = _getLetterIndex(property[0]);
      int secondCoord = _getLetterIndex(property[1]);
      coords.add(Pair<int, int>(firstCoord, secondCoord));
    }

    return coords;
  }

  /// Checks if a coordinate is in bounds of a board used by an SGF.
  /// TODO: Assumes that the game metadata properties will be parsed before
  /// the move propeties.
  bool _isMoveInBounds(Pair<int, int> coord) {
    return (coord != null &&
        coord.first >= 0 &&
        coord.second >= 0 &&
        coord.first < sgfTreeDescription.matchInfo.size.first &&
        coord.second < sgfTreeDescription.matchInfo.size.second);
  }

  /// Helpers function to handle manipulating stones in-place (e.g. handicaps or edits).
  void _addMoveToPosition(
      BoardTreeIterator position, Pair<int, int> coord, StoneColor color) {
    BoardState boardState = position.current;
    boardState.board.setAt(coord, color);
    boardState.stonesChanged
        .add(StoneStatus(color, coord, ModificationType.add));
  }

  void _removeMoveFromPosition(
      BoardTreeIterator position, Pair<int, int> coord) {
    BoardState boardState = position.current;
    StoneColor oldColor = boardState.board.getAt(coord);
    boardState.board.setAt(coord, StoneColor.empty());
    boardState.stonesChanged
        .add(StoneStatus(oldColor, coord, ModificationType.remove));
  }

  /// Parsers follow.

  void _sourceApplicationParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    // TODO(yakcyll): fix this parser!
    // Parser partialSourceApplicationParser = ((char('\\') & word()) |
    //     (word() | whitespace() | (char(']') | char(':')).not()));
    // debugPrint("the");
    // Parser sourceApplicationParser = partialSourceApplicationParser.plus() &
    //     char(':') &
    //     partialSourceApplicationParser.star();
    Parser partialSourceApplicationParser = word();
    Parser sourceApplicationParser = partialSourceApplicationParser.plus() &
        char(':') &
        partialSourceApplicationParser.star();
    if (sgfTreeDescription.fileHeader.sourceApplication == '' &&
        sourceApplicationParser.accept(properties[0])) {
      sgfTreeDescription.fileHeader.sourceApplication =
          _convertToSimpleText(properties[0]);
    }
  }

  void _fileEncodingParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.fileHeader.fileEncoding = properties[0];
  }

  void _gameModeParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.fileHeader.gameMode = int.parse(properties[0]);
  }

  void _fileFormatParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.fileHeader.fileFormat = int.parse(properties[0]);
  }

  void _variationModeParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    // TODO:?
  }

  void _gameDateParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    try {
      sgfTreeDescription.gameMetadata.gameDate = DateTime.parse(properties[0]);
    } on FormatException catch (e) {
      return;
    }
    // TODO: more error handling
  }

  void _atEventParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.gameMetadata.atEvent =
        _convertToSimpleText(properties[0]);
  }

  void _gameNameParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.gameMetadata.name = _convertToSimpleText(properties[0]);
  }

  void _extraInfoParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.gameMetadata.extraInfo = properties[0];
  }

  void _gameLocationParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.gameMetadata.location =
        _convertToSimpleText(properties[0]);
  }

  void _userParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.gameMetadata.user = _convertToSimpleText(properties[0]);
  }

  void _roundParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.gameMetadata.round = _convertToSimpleText(properties[0]);
  }

  void _gameSourceParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.gameMetadata.source =
        _convertToSimpleText(properties[0]);
  }

  void _blackNameParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.matchInfo.blackName =
        _convertToSimpleText(properties[0]);
  }

  void _blackRankParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.matchInfo.blackRank =
        _convertToSimpleText(properties[0]);
  }

  void _blackTeamParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.matchInfo.blackTeam =
        _convertToSimpleText(properties[0]);
  }

  void _whiteNameParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.matchInfo.whiteName =
        _convertToSimpleText(properties[0]);
  }

  void _whiteRankParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.matchInfo.whiteRank =
        _convertToSimpleText(properties[0]);
  }

  void _whiteTeamParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.matchInfo.whiteTeam =
        _convertToSimpleText(properties[0]);
  }

  void _boardSizeParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    Parser partialBoardSizeParser = (digit().plus()).flatten();
    int size =
        int.parse(partialBoardSizeParser.matchesSkipping(properties[0])[0]);
    sgfTreeDescription.matchInfo.size = Pair(size, size);
  }

  void _rulesetParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    // TODO: Convert to appropriate ruleset in code.
    sgfTreeDescription.matchInfo.ruleset = _convertToSimpleText(properties[0]);
  }

  void _gameResultParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    Parser resultParser = ((char('B') | char('W')) &
        char('+') &
        (char('T') |
            char('R') |
            (digit().plus() & (char('.') & digit().plus()).optional())));
    String resultText = _convertToSimpleText(properties[0]);
    sgfresult.Result newResult = sgfresult.Result();
    sgfTreeDescription.matchInfo.result = newResult;
    if (resultParser.accept(resultText)) {
      newResult.winner = StoneColor({'B': 1, 'W': 2}[resultText[0]]!, 2);
      newResult.reason =
          sgfresult.Result.translateReason(resultText.substring(2));
      if (newResult.reason == sgfresult.Reason.score) {
        newResult.score = double.parse(resultText.substring(2));
      }
    }
    // else newResult.reason == SGFResult.Result.unknown;
  }

  void _komiParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.matchInfo.komi = double.parse(properties[0]);
  }

  void _timePerPlayerParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.matchInfo.timePerPlayer =
        double.parse(_convertToSimpleText(properties[0]));
    if (position != null && position.isValid()) {
      position.current.timeLeft.fillRange(0, position.current.timeLeft.length,
          sgfTreeDescription.matchInfo.timePerPlayer);
    }
  }

  void _handicapParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    sgfTreeDescription.matchInfo.handicap = int.parse(properties[0]);
  }

  void _moveParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid() || ruleset == null) {
      return;
    }

    Pair<int, int> passMove = Pair(
        position.current.board.boardWidth, position.current.board.boardHeight);

    for (String c in properties) {
      List<Pair<int, int>> coords = _getCoordinatesFromProperty(c);
      Pair<int, int>? coord =
          coords.isNotEmpty && coords[0] != passMove ? coords[0] : null;
      // if (coord != null) debugPrint('Got a move coord (${coord.first}, ${coord.second})');
      // debugPrint('Board size (${sgfTreeDescription.matchInfo.size.first}, ${sgfTreeDescription.matchInfo.size.second})');
      StoneColor playerColor = StoneColor.fromCode(propId);
      if (coord == null || _isMoveInBounds(coord)) {
        // debugPrint('applying a move on coord ${coord}');
        BoardState newBoardState = ruleset
            .applyMove(position.current, coord, playerColor, inPlace: true)
          ..comment = ''
          ..moveNumber = position.current.moveNumber + 1;
      }
    }
  }

  void _timeLeftParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    Map<String, int> colorIndices = {'BL': 0, 'WL': 1};
    double newTimeLeft = double.parse(_convertToSimpleText(properties[0]));
    position.current.timeLeft[colorIndices[propId]!] = newTimeLeft;
  }

  void _moveSetupParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    for (String c in properties) {
      List<Pair<int, int>> coords = _getCoordinatesFromProperty(c);
      for (Pair<int, int> coord in coords) {
        if (_isMoveInBounds(coord)) {
          _addMoveToPosition(position, coord, StoneColor.fromCode(propId));
          position.current.edited = true;
        }
      }
    }
  }

  void _moveClearParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    for (String c in properties) {
      List<Pair<int, int>> coords = _getCoordinatesFromProperty(c);
      for (Pair<int, int> coord in coords) {
        if (_isMoveInBounds(coord)) {
          _removeMoveFromPosition(position, coord);
        }
      }
    }
  }

  void _playerToMoveParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if ((char('B') | char('W')).accept(properties[0])) {
      position?.current.colorToPlay =
          StoneColor((properties[0] == 'B' ? 1 : 2), 2);
    }
  }

  void _commentParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    // print('== found a comment ya nerd: \'${properties.join(' ')}\'');
    if (position != null && position.isValid()) {
      position.current.comment =
          properties.join(' ').replaceAll(RegExp(r'\\]'), ']');
    }
  }

  void _nodeNameParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position != null) {
      position.current.name = properties.join(' ');
    }
  }

  void _nodeValueParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    double nodeValue = double.parse(_convertToSimpleText(properties[0]));
    position.current.evaluationMetadata.value = nodeValue;
  }

  void _evenEvaluationParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null ||
        !position.isValid() ||
        position.current.evaluationMetadata.positionEvaluation != null) {
      return;
    }

    double evenEvaluation = double.parse(_convertToSimpleText(properties[0]));
    position.current.evaluationMetadata.positionEvaluation ??=
        Evaluation<PositionEvaluationType>.value(
            PositionEvaluationType.even, evenEvaluation);
  }

  void _blackBetterEvaluationParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null ||
        !position.isValid() ||
        position.current.evaluationMetadata.positionEvaluation != null) {
      return;
    }

    double blackBetterEvaluation =
        double.parse(_convertToSimpleText(properties[0]));
    position.current.evaluationMetadata.positionEvaluation ??=
        Evaluation<PositionEvaluationType>.value(
            PositionEvaluationType.blackBetter, blackBetterEvaluation);
  }

  void _whiteBetterEvaluationParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null ||
        !position.isValid() ||
        position.current.evaluationMetadata.positionEvaluation != null) {
      return;
    }

    double whiteBetterEvaluation =
        double.parse(_convertToSimpleText(properties[0]));
    position.current.evaluationMetadata.positionEvaluation ??=
        Evaluation<PositionEvaluationType>.value(
            PositionEvaluationType.whiteBetter, whiteBetterEvaluation);
  }

  void _hotspotParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    double hotspotEvaluation =
        double.parse(_convertToSimpleText(properties[0]));
    position.current.evaluationMetadata.hotspot = hotspotEvaluation;
  }

  void _unclearEvaluationParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null ||
        !position.isValid() ||
        position.current.evaluationMetadata.positionEvaluation != null) {
      return;
    }

    double unclearEvaluation =
        double.parse(_convertToSimpleText(properties[0]));
    position.current.evaluationMetadata.positionEvaluation ??=
        Evaluation<PositionEvaluationType>.value(
            PositionEvaluationType.unclear, unclearEvaluation);
  }

  void _badMoveParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null ||
        !position.isValid() ||
        position.current.evaluationMetadata.moveEvaluation != null) {
      return;
    }

    double badMoveEvaluation =
        double.parse(_convertToSimpleText(properties[0]));
    position.current.evaluationMetadata.moveEvaluation ??=
        Evaluation<MoveEvaluationType>.value(
            MoveEvaluationType.badMove, badMoveEvaluation);
  }

  void _doubtfulMoveParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null ||
        !position.isValid() ||
        position.current.evaluationMetadata.moveEvaluation != null) {
      return;
    }

    position.current.evaluationMetadata.moveEvaluation ??=
        Evaluation<MoveEvaluationType>.value(MoveEvaluationType.doubtful, 0.0);
  }

  void _interestingMoveParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null ||
        !position.isValid() ||
        position.current.evaluationMetadata.moveEvaluation != null) {
      return;
    }

    position.current.evaluationMetadata.moveEvaluation ??=
        Evaluation<MoveEvaluationType>.value(
            MoveEvaluationType.interesting, 0.0);
  }

  void _tesujiParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null ||
        !position.isValid() ||
        position.current.evaluationMetadata.moveEvaluation != null) {
      return;
    }

    double tesujiEvaluation = double.parse(_convertToSimpleText(properties[0]));
    position.current.evaluationMetadata.moveEvaluation ??=
        Evaluation<MoveEvaluationType>.value(
            MoveEvaluationType.tesuji, tesujiEvaluation);
  }

  void _arrowParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    for (String c in properties) {
      List<Pair<int, int>> coords = _getCoordinatesFromProperty(c);
      Pair<int, int> arrowBegin = coords.first;
      Pair<int, int> arrowEnd = coords.last;
      position.current.markup.arrows.add(Pair(arrowBegin, arrowEnd));
    }
  }

  void _circleParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    for (String c in properties) {
      Pair<int, int> coord = _getCoordinatesFromProperty(c)[0];
      position.current.markup.circles.add(coord);
    }
  }

  void _labelParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    for (String property in properties) {
      if ((pattern('[a-zA-Z][a-zA-Z]') &
              char(':') &
              ((whitespace() |
                      anyCharExcept(']\\') |
                      (char('\\') & char(']')) |
                      char('\\'))
                  .star()))
          .accept(property)) {
        Pair<int, int> coord =
            _getCoordinatesFromProperty(property.substring(0, 2))[0];
        String label = property.substring(3);
        position.current.markup.labels.add(Pair(coord, label));
      }
    }
  }

  void _lineParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    for (String c in properties) {
      List<Pair<int, int>> coords = _getCoordinatesFromProperty(c);
      Pair<int, int> lineBegin = coords.first;
      Pair<int, int> lineEnd = coords.last;
      position.current.markup.arrows.add(Pair(lineBegin, lineEnd));
    }
  }

  void _crossParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    for (String c in properties) {
      Pair<int, int> coord = _getCoordinatesFromProperty(c)[0];
      position.current.markup.crosses.add(coord);
    }
  }

  void _squareParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    for (String c in properties) {
      Pair<int, int> coord = _getCoordinatesFromProperty(c)[0];
      position.current.markup.squares.add(coord);
    }
  }

  void _triangleParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    if (position == null || !position.isValid()) {
      return;
    }

    for (String c in properties) {
      Pair<int, int> coord = _getCoordinatesFromProperty(c)[0];
      position.current.markup.triangles.add(coord);
    }
  }

  void _toBeImplementedParser(String propId, List<String> properties,
      {CommonTwoPlayerRules? ruleset, BoardTreeIterator? position}) {
    dev.log(
        '== Encountered property ${propId}, which is not currently supported.');
  }
}

import 'package:sgfparser/goban/board.dart' show Board;
import 'package:sgfparser/goban/boardTreeIterator.dart' show BoardTreeIterator;
import 'package:sgfparser/goban/stone.dart' show StoneColor;
import 'package:sgfparser/utils/pair.dart' show Pair;

typedef MoveValidator = bool Function(
    BoardTreeIterator position, Pair coord, StoneColor color);

/// A base template class for rulesets.
class Rules {
  List<MoveValidator> moveValidators = [];
  static Pair<double, double> count(Board position) {
    return Pair(0.0, 0.0);
  }
}

import 'package:sgfparser/goban/boardTreeNode.dart' show BoardTreeNode;
import 'package:sgfparser/goban/board.dart' show Board;
import 'package:sgfparser/goban/boardState.dart' show BoardState;
import 'package:sgfparser/utils/ticIterator.dart' show TicIterator;

import 'package:flutter/foundation.dart';

/// Class representing an iterator, or a viewport, over a tree of board positions.
/// Primary way to manipulate a board tree. Extending the basic project iterator,
/// implements a number of helper functions to manipulate a tree at position
/// it points to. Serves as a gateway for components used to view or manipulate
/// the board.
class BoardTreeIterator extends TicIterator<BoardState, BoardTreeNode> {
  BoardTreeIterator(BoardTreeNode? currentNode, int positionIndex)
      : super(currentNode, positionIndex);

  /// Copying constructor of an iterator; used for implementing operator+ and operator-.
  /// WARNING: The new iterator will NOT inherit the listeners of the old one.
  BoardTreeIterator.copy(TicIterator other) : super.copy(other);

  /// Creates a variation with a supplied position at the position it points to.
  void insertVariation(BoardState state) => currentNode?.insertVariation(state,
      afterPositionIndex: currentPositionIndex);

  /// Counts the passes before the position it points to.
  /// If the iterator points to the beginning of the sequence of the root,
  /// then the pass count is 0. Otherwise, invokes the pass counting method
  /// of the node it points to.
  int countPreceedingPasses() => this == (this - 1)
      ? 0
      : currentNode!.countPassesBeforePosition(
          current.board, (this - 1).getCurrentPositionInTree().second);

  /// Helper function to find a position preceding the one the iterator
  /// points to. Since the iterator can be pointing to a non-existing board state
  /// descriptor (e.g. because the sequence is empty), this function facilitates
  /// searching for the latest valid position on the path to the beginning of
  /// the root's sequence.
  BoardTreeIterator? getPreviousValidPosition() {
    BoardTreeIterator temp = BoardTreeIterator.copy(this);
    while (temp.movePrevious() && !temp.isValid()) {}
    if (!temp.isValid()) return null;
    return temp;
  }

  BoardTreeIterator? getLastNoneditedPosition() {
    BoardTreeIterator temp = BoardTreeIterator.copy(this);
    while ((temp.current.edited || !temp.isValid()) && temp.movePrevious()) {}
    if (!temp.isValid()) return null;
    return temp;
  }

  /// Checks if a board position is repeated anywhere on a path before position
  /// pointed at by this iterator and the first position in the tree.
  /// Can be used as a superko validator when applied to (this - 2).
  bool checkIfPositionExistsInHistory(Board position) {
    BoardTreeIterator head = BoardTreeIterator.copy(this);
    head.movePrevious();
    do {
      if (head.current.board == position) {
        return false;
      }
    } while (head.movePrevious());

    return true;
  }

  /// Getter for the board state preceding the current one.
  BoardState get previous => (this - 1).current;

  /// Getter for the move number for position currently pointed to.
  int get moveNumber => currentNode!
      .getMoveNumberForPosition(positionIndex: currentPositionIndex);

  /// Function used to change the position the iterator points to.
  @override
  void setPosition(BoardTreeNode? node, int positionIndex) {
    super.setPosition(node, positionIndex);
  }

  /// Function used to move the iterator to the first position in the sequence.
  @override
  bool moveFirst() {
    bool result = super.moveFirst();
    return result;
  }

  /// Function used to move the iterator to the first position in the sequence.
  @override
  bool moveLast() {
    bool result = super.moveLast();
    return result;
  }

  /// Function used to move the iterator to the next position in the sequence.
  @override
  bool moveNext() {
    bool result = super.moveNext();
    return result;
  }

  /// Function used to move the iterator to the previous position in the sequence.
  @override
  bool movePrevious() {
    bool result = super.movePrevious();
    return result;
  }

  /// Function used to obtain an iterator pointing to a position `other` moves
  /// forward on the sequence.
  @override
  BoardTreeIterator operator +(other) {
    BoardTreeIterator newIter = BoardTreeIterator.copy(super + (other));
    return newIter;
  }

  /// Function used to obtain an iterator pointing to a position `other` moves
  /// back on the sequence.
  @override
  BoardTreeIterator operator -(other) {
    BoardTreeIterator newIter = BoardTreeIterator.copy(super - (other));
    return newIter;
  }

  /// Function used to compare iterators. The defining characteristics of
  /// an iterator are the node pointer and index in sequence, so these
  /// are compared with `other`.
  bool pointsToTheSamePositionAs(BoardTreeIterator other) {
    return currentNode == other.currentNode &&
        currentPositionIndex == other.currentPositionIndex;
  }
}

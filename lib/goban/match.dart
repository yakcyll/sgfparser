import 'package:sgfparser/sgf/gameMetadata.dart' show GameMetadata;
import 'package:sgfparser/sgf/matchInfo.dart' show MatchInfo;
import 'package:sgfparser/goban/gameState.dart' show GameState;
import 'package:sgfparser/goban/player.dart' show Player;

/// Class representing a match, complete with a game state and its metadata.
class Match {
  late GameMetadata metadata;
  late MatchInfo matchInfo;
  late GameState gameState;

  late List<Player>
      players; /* TODO: Make sure the players are added in order of play. */

  // Potentially redundant info.
  late int passCount = 0;

  Match() {
    players = <Player>[];

    matchInfo = MatchInfo();
    metadata = GameMetadata();
    gameState = GameState();
  }

  factory Match.forGame(MatchInfo gameInfo) {
    return Match()..matchInfo = gameInfo;
  }

  Match.copy(Match other) {
    players = List.from(other.players);
    matchInfo = MatchInfo.copy(other.matchInfo);
    metadata = GameMetadata.copy(other.metadata);
    gameState = GameState.copy(other.gameState);
  }
}

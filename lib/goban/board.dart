import 'package:sgfparser/goban/stone.dart' show StoneColor;
import 'package:sgfparser/utils/pair.dart' show Pair;

/// Class representing a board position.
/// Basic unit of game data, contains a list of fields represented by stone colors.
class Board {
  int boardWidth = 19;
  int boardHeight = 19;

  List<StoneColor> fields = [];

  Board(this.boardWidth, this.boardHeight) {
    fields = List.generate(boardWidth * boardHeight, (index) {
      return StoneColor.empty();
    });
  }

  /// Copying constructor. Clones the fields from another position.
  Board.copy(Board another) {
    boardWidth = another.boardWidth;
    boardHeight = another.boardHeight;

    fields = List.generate(another.boardWidth * another.boardHeight,
        (idx) => StoneColor.copy(another.fields[idx]));
  }

  /// Getter of a stone at position `coord`. Returns a _clone_ of that stone object.
  StoneColor getAt(Pair coord) {
    return StoneColor.copy(fields[coord.second * boardWidth + coord.first]);
  }

  /// Setter of a stone of color `color` at position `coord`. Overwrites the stone object at that position.
  void setAt(Pair coord, StoneColor color) {
    assert(coord != null && color != null);
    fields[coord.second * boardWidth + coord.first] = StoneColor.copy(color);
  }

  /// Helper function returning a list of coordinates neighboring a field at `coord`,
  /// taking board size into consideration.
  List<Pair> getNeighbors(Pair coord) {
    List<Pair> neighboringPairs = [];

    if (coord.first > 0)
      neighboringPairs.add(Pair(coord.first - 1, coord.second));
    if (coord.first < boardWidth - 1)
      neighboringPairs.add(Pair(coord.first + 1, coord.second));
    if (coord.second > 0)
      neighboringPairs.add(Pair(coord.first, coord.second - 1));
    if (coord.second < boardHeight - 1)
      neighboringPairs.add(Pair(coord.first, coord.second + 1));

    return neighboringPairs;
  }

  /// Algorithm for finding liberties of the group including the stone at `coord`.
  ///
  /// Finds the stones of the group and checks their neighbors for empty fields.
  ///
  /// Returns a set of coordinates of empty fields neighboring the group including
  /// the stone at `coord`.
  Set<Pair> getLiberties(Pair coord) {
    Set<Pair> liberties = {};

    Set<Pair> group = getChainPoints(coord);
    for (Pair stone in group) {
      List<Pair> neighbors = getNeighbors(stone);

      for (Pair neighbor in neighbors) {
        if (getAt(neighbor) == StoneColor.empty()) {
          liberties.add(neighbor);
        }
      }
    }

    return liberties;
  }

  /// Algorithm for finding stones of the group which includes the stone at `coord`.
  ///
  /// Clones the current position and performs a flood-filling algorithm starting
  /// at `coord`, recursively looking for stones of the same color among neighbors.
  ///
  /// Returns a set of coordinates of stones of the group including the stone
  /// at `coord`.
  Set<Pair> getChainPoints(Pair coord) {
    Set<Pair> stones = {};
    stones.add(coord);

    Board checkBoard = Board.copy(this);
    StoneColor myColor = StoneColor.copy(getAt(coord));
    StoneColor floodFillColor = myColor.next();
    checkBoard.setAt(coord, floodFillColor);

    List<Pair> neighbors = checkBoard.getNeighbors(coord);

    for (Pair neighbor in neighbors) {
      if (checkBoard.getAt(neighbor) == myColor) {
        stones.addAll(checkBoard.getChainPoints(neighbor));
      }
    }

    return stones;
  }

  /// Helper function for checking if a stone at `coord` is in atari
  /// (i.e. has one liberty).
  bool isInAtari(Pair coord, {bool killed = false}) {
    return getAt(coord) != StoneColor.empty() &&
        getLiberties(coord).length == (killed ? 0 : 1);
  }

  /// Compares another board position to this one.
  ///
  /// Returns two vectors of representations of stones. The first vector
  /// represents stones that are included in this and missing in other.
  /// The second vector represents stones that are missing in this included
  /// in other. Both vectors store pairs of coordinates and stone colors.
  Pair<List<Pair<Pair, StoneColor>>, List<Pair<Pair, StoneColor>>> compareWith(
      Board other) {
    if (other == null) {
      return Pair([], []);
    }

    if (boardWidth != other.boardWidth || boardHeight != other.boardHeight) {
      return Pair([], []);
    }

    List<Pair<Pair, StoneColor>> stonesHere = [];
    List<Pair<Pair, StoneColor>> stonesThere = [];

    for (int i = 0; i < boardWidth * boardHeight; ++i) {
      Pair coord = Pair(i % boardWidth, i ~/ boardWidth);
      StoneColor stoneHere = getAt(coord);
      StoneColor stoneThere = other.getAt(coord);

      if (stoneHere != stoneThere) {
        if (stoneHere != StoneColor.empty()) {
          stonesHere.add(Pair(coord, stoneHere));
        }
        if (stoneThere != StoneColor.empty()) {
          stonesThere.add(Pair(coord, stoneThere));
        }
      }
    }
    return Pair(stonesHere, stonesThere);
  }

  /// Equation operator used to compare boards.
  ///
  /// Returns true if there are no stones different between this
  /// and the other board.
  @override
  bool operator ==(other) {
    if (other is! Board) {
      return false;
    }

    Pair<List, List> comparison = compareWith(other);
    if (comparison.first.isEmpty && comparison.second.isEmpty) {
      return true;
    } else {
      return false;
    }
  }
}

import 'package:sgfparser/goban/board.dart' show Board;
import 'package:sgfparser/goban/boardState.dart' show BoardState;
import 'package:sgfparser/goban/boardTreeIterator.dart' show BoardTreeIterator;
import 'package:sgfparser/utils/ticIterator.dart' show TicNodeTemplate;
import 'dart:collection';

/// Class representing game/board position history tree. A counterpart
/// to the SGF Tree Node class, used by board-manipulating components to represent
/// board state history. Mirrors the move history described in an SGF using
/// complete positions. This representation makes it easier to iterate over
/// and manipulate positions and trees.
class BoardTreeNode extends TicNodeTemplate<BoardState>
    with IterableMixin<BoardState> {
  BoardTreeNode() {
    parent = null;
    sequence = [];
    children = [];
  }

  /// Copying constructor, creating a new tree with a clone of a subtree
  /// with the root at `other`.
  BoardTreeNode.copy(BoardTreeNode other) {
    parent = null; // Cloning the parent would create another copy of this
    // node, so the caller has to assign the parent separately.

    sequence = List<BoardState>.generate(other.sequence.length,
        (index) => BoardState.copy(other.sequence[index]));

    children = List<BoardTreeNode>.generate(
        other.children.length,
        (index) => BoardTreeNode.copy(other.children[index] as BoardTreeNode)
          ..parent = this);
  }

  /// Getter for an iterator pointing at the beginning of the sequence in this node.
  @override
  Iterator<BoardState> get iterator => BoardTreeIterator(this, 0);

  /// Adds a position to the end of the sequence of this node.
  /// The positions are first wrapped in a board state descriptor
  /// along with an optional captured stone count for easier tree traversal
  /// during play and editing.
  void addPosition(BoardState state) {
    BoardState newBoardState = BoardState.copy(state);
    // TODO: redundant code
    // if (sequence.isNotEmpty)
    //   newBoardState.capturedStoneCounts = List.from(sequence.last.capturedStoneCounts);
    // else
    //   newBoardState.capturedStoneCounts = List.generate(state.board.fields[0].maxColor, (i) => 0);

    // TODO: Given how the copying constructor for board states works,
    // TODO: this code does nothing. Verify this.
    // if (state.capturedStoneCounts != null) {
    //   if (newBoardState.capturedStoneCounts.isEmpty) {
    //     for (int count in state.capturedStoneCounts) {
    //       newBoardState.capturedStoneCounts.add(count);
    //     }
    //   } else {
    //     for (int i = 0; i < state.capturedStoneCounts.length; ++i) {
    //       newBoardState.capturedStoneCounts[i] = state.capturedStoneCounts[i];
    //     }
    //   }
    // }

    sequence.add(newBoardState);
  }

  /// Creates a new variation after a selected position.
  /// If the preceding position is the last in the sequence, the case
  /// is passed to addPosition. Otherwise, the node is split in two;
  /// the first - ancestor - keeps the positions up to the preceding
  /// position and takes the second - descendant - as its child,
  /// whereas the descendant takes the rest of positions as its own
  /// sequence and previous children of the ancestor. The new position
  /// is then put in a new node and added to children of the ancestor.
  void insertVariation(BoardState state,
      {Board? afterPosition, int? afterPositionIndex}) {
    if (afterPosition != null) {
      afterPositionIndex = sequence.indexOf(afterPosition as BoardState);
    }

    // a ??= b assigns b to a if a is null; neat.
    afterPositionIndex ??= sequence.length - 1;

    if (afterPositionIndex == sequence.length - 1) {
      addPosition(state);
    } else {
      // Split the tree here.
      List<BoardState> sequencePrior =
          sequence.sublist(0, afterPositionIndex + 1);
      List<BoardState> sequenceAfter = sequence.sublist(afterPositionIndex);

      BoardTreeNode splitNode = BoardTreeNode();
      splitNode.parent = this;
      splitNode.sequence = sequenceAfter;
      splitNode.children = children;

      BoardTreeNode newNode = BoardTreeNode();
      newNode.parent = this;
      newNode.sequence = [BoardState.copy(sequenceAfter[0])];

      newNode.addPosition(state);

      sequence = sequencePrior;
      children = [splitNode, newNode];
    }
  }

  /// Prunes the tree up to the selected position.
  void pruneTreeTo({required Board toPosition, required int toPositionIndex}) {
    if (toPosition != null) {
      toPositionIndex = sequence.indexOf(toPosition as BoardState);
    }

    children.clear();
    sequence = sequence.sublist(0, toPositionIndex + 1);
  }

  /// Helper function used to verify if the tree starting at this node
  /// contains a board position. This is done by checking the local sequence
  /// and recursively checking the children.
  @override
  bool contains(Object? position) {
    if (position is! Board) {
      return false;
    }

    if (sequence.contains(position)) {
      return true;
    } else {
      for (BoardTreeNode subsequence in children.cast<BoardTreeNode>()) {
        if (subsequence.contains(position)) {
          return true;
        }
      }
    }

    return false;
  }

  /// Returns the move number for a position.
  /// Counts the number of positions between the supplied one and the first
  int getMoveNumberForPosition({BoardState? position, int? positionIndex}) {
    assert(position != null || positionIndex != null);

    if (position != null) {
      positionIndex = sequence.indexOf(position);
    }

    return sequence[positionIndex!].moveNumber;
  }

  /// Counts the number of pass moves performed before the position at supplied
  /// index appears in the sequence. Assumes a pass move results in the same
  /// position being repeated in a sequence and compares the supplied position
  /// with those before the index.
  int countPassesBeforePosition(Board repeatedPosition, int positionIndex) {
    int passCount = 0;
    while (positionIndex >= 0) {
      if (repeatedPosition == sequence[positionIndex].board) {
        passCount += 1;
      } else {
        return passCount;
      }

      positionIndex -= 1;
    }

    if (parent != null) {
      return (parent as BoardTreeNode).countPassesBeforePosition(
              repeatedPosition, parent!.sequence.length - 1) +
          passCount;
    } else {
      return passCount;
    }
  }
}

import 'package:sgfparser/goban/match.dart' show Match;
import 'package:sgfparser/goban/stone.dart' show StoneColor;

/// Class representing a player descriptor.
class Player {
  late String name;
  late StoneColor color;
  late double score;

  late bool ready;

  late Match activeMatch;

  Player(Match match, StoneColor color) {
    activeMatch = match;
    this.color = color;
    score = 0.0;
    ready = false;
  }

  factory Player.named(Match match, StoneColor color, String name) {
    var namedPlayer = Player(match, color);
    namedPlayer.name = name;
    return namedPlayer;
  }
}

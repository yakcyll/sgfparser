import 'package:sgfparser/goban/board.dart' show Board;
import 'package:sgfparser/goban/boardState.dart' show BoardState;
import 'package:sgfparser/goban/boardTreeIterator.dart' show BoardTreeIterator;
import 'package:sgfparser/goban/rules.dart' show Rules;
import 'package:sgfparser/goban/stone.dart'
    show StoneColor, StoneStatus, ModificationType;
import 'package:sgfparser/utils/pair.dart' show Pair;

import 'package:flutter/foundation.dart';

/// CommonTwoPlayerRules - class representing the logic of playing on a board.
/// Extends Rules - a template for classes following this strategy pattern - with basic Trump-Taylor logic.
/// Currently a placeholder for common rulesets (e.g. Japanese, Chinese, AGA).
class CommonTwoPlayerRules extends Rules {
  bool allowsSuicide = false;

  CommonTwoPlayerRules() : super() {
    moveValidators.add(isValidMove);
  }

  // TODO: TBI
  Pair<double, double> count(Board position) {
    return Pair(0.0, 0.0);
  }

  /// Creates a descriptor for a position resulting from playing a `color` move at `coord`.
  /// Does not check whether the move is valid; will overwrite existing moves.
  BoardState applyMove(BoardState position, Pair? coord, StoneColor color,
      {bool inPlace = false}) {
    BoardState newPosition = inPlace ? position : BoardState.copy(position);
    Set<Pair> capturedStones;

    newPosition.stonesChanged += [
      StoneStatus(color, coord,
          coord != null ? ModificationType.add : ModificationType.none)
    ];

    if (coord != null) {
      newPosition.board.setAt(coord, color);

      capturedStones = searchForPrisoners(newPosition.board, coord);
      for (Pair field in capturedStones) {
        // debugPrint('found a prisoner on coord ${field}');
        StoneColor oldColor = newPosition.board.getAt(field);
        newPosition.board.setAt(field, StoneColor.empty());
        newPosition.stonesChanged += [
          StoneStatus(oldColor, field, ModificationType.remove)
        ];
      }

      // Here, turn becomes a proxy for the player count;
      // how to pass to/store this information in GameState better?
      newPosition.capturedStoneCounts[color.stoneColor - 1] +=
          capturedStones.length;
    }

    return newPosition;
  }

  /// Validator for moves in specific positions.
  /// Checks for existing moves, kos and suicides. Does not manipulate the board state.
  bool isValidMove(
      BoardTreeIterator currentPosition, Pair coord, StoneColor color) {
    StoneColor opponentColor = color.next();

    Board currentMatchPosition = currentPosition.current.board;
    if (currentMatchPosition.getAt(coord) != StoneColor.empty()) {
      return false;
    }

    // Single point ko variant.
    // Pseudo-code for C++/CX.

    /* if (coord.Equals(((Go19Match^)m)->koPoint)) {
	  	for (Point neighbor : getNeighbors(coord, Point(m->board->sBoardWidth, m->board->sBoardHeight))) {
	  		ygcBoard^ b = ref new ygcBoard(m->board);
	  		if (getChainPoints(b, neighbor)->Size == 1) {
	  			if (inAtari(m->board, neighbor))
	  				return false;
	  		}
	  	}
	  } */

    // Superko variant, less performant.
    if (currentPosition
        .checkIfPositionExistsInHistory(currentPosition.current.board)) {
      return false;
    }

    // The move is a suicide if it has no liberties and does not take an opponent's stone;
    // in other words, it is not a suicide if it has a liberty, the group it connects to
    // has liberties or it takes an opponent stone.
    bool suicide = true;
    for (Pair neighbor in currentMatchPosition.getNeighbors(coord)) {
      if (currentMatchPosition.getAt(neighbor) == StoneColor.empty()) {
        suicide = false;
      } else if (currentMatchPosition.getAt(neighbor) == color &&
          !currentMatchPosition.isInAtari(neighbor)) {
        suicide = false;
      } else if (currentMatchPosition.getAt(neighbor) == opponentColor &&
          currentMatchPosition.isInAtari(neighbor)) {
        suicide = false;
      }
    }

    if (suicide) {
      return false;
    }

    return true;
  }

  /// Algorithm for finding prisoners taken by move at `coord` on `position`.
  /// Checks the neighbors of `coord` for no-liberty groups.
  /// Returns a set of coordinates of stones without liberties, to be removed
  /// from the board.
  Set<Pair> searchForPrisoners(Board position, Pair coord) {
    Set<Pair> prisoners = {};
    Board currentPosition = Board.copy(position);

    StoneColor opponentColor =
        StoneColor.copy(currentPosition.getAt(coord)).next();

    for (Pair neighbor in currentPosition.getNeighbors(coord)) {
      if (currentPosition.getAt(neighbor) == opponentColor &&
          currentPosition.isInAtari(neighbor, killed: true)) {
        prisoners.addAll(currentPosition.getChainPoints(neighbor));
      }
    }

    // Single point ko variant code.
    /* if (prisoners.length == 1)
	  	match.koPoint = prisoners[0];
	  else
	  	match.koPoint = Pair(-1, -1); */

    // if (prisoners.isNotEmpty)
    //   print("Found ${prisoners.length} prisoners taken with (${coord.first}, ${coord.second}).");

    return prisoners;
  }
}

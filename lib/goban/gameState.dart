import 'package:sgfparser/goban/rulesets/commonRules.dart';
import 'package:sgfparser/goban/boardTreeNode.dart' show BoardTreeNode;
import 'package:sgfparser/goban/boardTreeIterator.dart' show BoardTreeIterator;
import 'package:sgfparser/goban/board.dart' show Board;
import 'package:sgfparser/goban/boardState.dart' show BoardState;
import 'package:sgfparser/goban/stone.dart' show StoneColor;
import 'package:sgfparser/utils/pair.dart' show Pair;

class GameState {
  late CommonTwoPlayerRules ruleset;
  late BoardTreeNode positions;
  late BoardTreeIterator currentPosition;

  int moveCount = 0;
  late StoneColor turn;

  GameState() {
    ruleset = CommonTwoPlayerRules();
    clearState();
    _setRulesetBasedOnMatchInfo();
  }

  /// Reset function for the game state. Inserts an empty position in the position tree.
  void clearState() {
    positions = BoardTreeNode();
    positions.addPosition(BoardState.fromBoard(Board(19, 19)));
    currentPosition = BoardTreeIterator(positions, 0);
    turn = StoneColor.empty()..stoneColor = 1;
    moveCount = 0;
  }

  /// Copying constructor which clones the board tree and copies the game state.
  GameState.copy(GameState other) {
    ruleset = other.ruleset;
    positions = BoardTreeNode.copy(other.positions); // TODO: profile
    currentPosition = BoardTreeIterator(positions, 0);
    turn = StoneColor.copy(other.turn);
    moveCount = other.moveCount;
  }

  /// Internal function to obtain a ruleset based on the description from the game
  /// metadata.
  void _setRulesetBasedOnMatchInfo() {
    /* TODO: Take the ruleset from the SGF and approximate the ruleset. */
    ruleset = CommonTwoPlayerRules();
  }

  /// Returns the player count based on the number of possible players as specified
  /// in the stone descriptor.
  int getPlayerCount() {
    return turn.maxColor;
  }

  /// Makes a play from the position the current iterator points to for
  /// the current color. By default, uses move validators from the ruleset
  /// to check if the move at `coord` can be performed.
  bool play(Pair coord, {bool validate = true}) {
    if (validate && !isMoveValid(coord, turn)) {
      return false;
    }

    makeMove(currentPosition, coord, turn);
    turn = turn.next();
    currentPosition.moveNext();

    return true;
  }

  /// Performs a move in the current match and updates the variation tree.
  /// Assumes that the requested move is valid on the supplied position.
  void makeMove(BoardTreeIterator fromPosition, Pair coord, StoneColor color) {
    BoardState newPosition =
        ruleset.applyMove(fromPosition.current, coord, color);

    moveCount += 1;

    // debugPrint("MAKING A MOVE BOWSER");
    // debugPrint("iterator is ${fromPosition.getCurrentPositionInTree().first.sequence.length == fromPosition.getCurrentPositionInTree().second + 1 ? "" : "not"} at the end of the seuqence");
    // debugPrint("sequence length before: ${fromPosition.getCurrentPositionInTree().first.sequence.length}");
//
    // debugPrint("initial position: (${fromPosition.getCurrentPositionInTree().first.sequence.toString()}, ${fromPosition.getCurrentPositionInTree().second})");

    fromPosition.insertVariation(newPosition);

    // debugPrint("new position: (${(fromPosition + 1).getCurrentPositionInTree().first.sequence.toString()}, ${(fromPosition + 1).getCurrentPositionInTree().second})");
    //
    // debugPrint("sequence length after: ${fromPosition.getCurrentPositionInTree().first.sequence.length}");
  }

  /// Performs a pass in the current match.
  /// Returns true if all players have consecutively passed, false otherwise.
  bool pass({int count = 1}) {
    if (count >= turn.maxColor - 1) {
      return false;
    }

    int subsequentPassCount = currentPosition.countPreceedingPasses();
    while (count > 0) {
      --count;
      BoardState repeatedPosition = currentPosition.current;
      currentPosition.insertVariation(repeatedPosition);
      ++moveCount;
      subsequentPassCount += 1;
      turn = turn.next();
    }

    return subsequentPassCount < getPlayerCount();
  }

  /// Method implementing the undo functionality. Rewinds the current position
  /// iterator and reverts other metadata changes to the game state.
  bool undoLastMove() {
    if (moveCount == 0) {
      return false;
    }

    turn = turn.previous();

    moveCount -= 1;

    currentPosition--;

    return true;
  }

  /// Helper function applying move validators from the ruleset to the current
  /// position.
  bool isMoveValid(Pair coord, StoneColor color) {
    bool retval = true;

    ruleset.moveValidators.forEach((validator) {
      if (!validator(currentPosition, coord, color)) {
        retval = false;
      }
    });

    return retval;
  }

  /// Helper function checking if the field at `coord` has a stone on it on
  /// the current position.
  bool isFieldTaken(Pair coord) {
    return getStone(coord) != StoneColor.empty();
  }

  /// Helper function retrieving a stone at `coord` from the current position.
  StoneColor getStone(Pair coord) {
    return currentPosition.current.board.getAt(coord);
  }

  /// Helper function returning the captured stone count for the player
  /// playing `color` stones at the current position in game's history.
  int getCaptureCount(StoneColor color) {
    Pair positionTuple = currentPosition.getCurrentPositionInTree();
    return positionTuple.first.captureCounts[positionTuple.second]
        [color.stoneColor - 1];
  }
}

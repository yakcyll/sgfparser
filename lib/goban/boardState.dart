import 'package:sgfparser/goban/board.dart' show Board;
import 'package:sgfparser/goban/stone.dart' show StoneColor, StoneStatus;
import 'package:sgfparser/utils/pair.dart';

/// Class representing a board position operator along with match position
/// metadata, including the captured stone counts.
class BoardState {
  late Board board;
  late List<int> capturedStoneCounts;
  late List<double> timeLeft;
  late List<StoneStatus> stonesChanged;
  late String name;
  late String comment;
  late EvaluationMetadata evaluationMetadata;
  late PositionMarkup markup;
  late int moveNumber;
  late StoneColor? colorToPlay;
  late bool edited;

  /// 'Default' constructor based on an empty position.
  BoardState(int boardWidth, int boardHeight, [int moveNumber = 0]) {
    board = Board(boardWidth, boardHeight);
    capturedStoneCounts = [];
    timeLeft = [];
    stonesChanged = [];
    name = '';
    comment = '';
    evaluationMetadata = EvaluationMetadata();
    markup = PositionMarkup();
    this.moveNumber = moveNumber;
    colorToPlay = null;
    edited = false;
  }

  /// Constructor cloning an existing board position.
  BoardState.fromBoard(Board position, [int moveNumber = 0]) {
    board = Board.copy(position);
    capturedStoneCounts = List<int>.filled(position.fields[0].maxColor, 0);
    timeLeft = List<double>.filled(position.fields[0].maxColor, 0.0);
    stonesChanged = [];
    name = '';
    comment = '';
    evaluationMetadata = EvaluationMetadata();
    markup = PositionMarkup();
    this.moveNumber = moveNumber;
    colorToPlay = null;
    edited = false;
  }

  /// Copying constructor, creating a clone of a state including position metadata.
  BoardState.copy(BoardState other) {
    board = Board.copy(other.board);
    capturedStoneCounts = List<int>.from(other.capturedStoneCounts);
    timeLeft = List<double>.from(other.timeLeft);
    stonesChanged = [];
    name = '';
    comment = '';
    evaluationMetadata = EvaluationMetadata();
    markup = PositionMarkup();
    moveNumber = other.moveNumber;
    colorToPlay = other.colorToPlay;
    edited = other.edited;
  }

  /// Equation operator comparing boards.
  /// Returns true if board positions are the same in this and the other state;
  /// ignores other position metadata.
  @override
  bool operator ==(other) {
    if (other is! BoardState && other is! Board) {
      return false;
    }

    if (other is BoardState) {
      return board == other.board;
    } else {
      return board == other;
    }
  }
}

enum PositionEvaluationType { even, blackBetter, whiteBetter, unclear }

enum MoveEvaluationType { badMove, doubtful, interesting, tesuji }

class Evaluation<T> {
  late T type;
  late double value;

  Evaluation.value(this.type, this.value);
  Evaluation.copy(Evaluation other) {
    type = other.type;
    value = other.value;
  }
}

class EvaluationMetadata {
  late double value = 0.0;
  late double hotspot = 0.0;
  late Evaluation<PositionEvaluationType>? positionEvaluation;
  late Evaluation<MoveEvaluationType>? moveEvaluation;

  EvaluationMetadata();
  EvaluationMetadata.forPosition(this.positionEvaluation);
  EvaluationMetadata.forMove(this.moveEvaluation);
  EvaluationMetadata.copy(EvaluationMetadata other) {
    value = other.value;
    hotspot = other.hotspot;
    positionEvaluation = other.positionEvaluation != null
        ? Evaluation<PositionEvaluationType>.copy(other.positionEvaluation!)
        : null;
    moveEvaluation = other.moveEvaluation != null
        ? Evaluation<MoveEvaluationType>.copy(other.moveEvaluation!)
        : null;
  }

  bool isEvaluated() {
    return value != 0.0 ||
        hotspot != 0.0 ||
        positionEvaluation != null ||
        moveEvaluation != null;
  }
}

class PositionMarkup {
  List<Pair<Pair<int, int>, Pair<int, int>>> arrows = [];
  List<Pair<Pair<int, int>, Pair<int, int>>> lines = [];
  List<Pair<Pair<int, int>, String>> labels = [];
  List<Pair<int, int>> circles = [];
  List<Pair<int, int>> crosses = [];
  List<Pair<int, int>> squares = [];
  List<Pair<int, int>> triangles = [];
}

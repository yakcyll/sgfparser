import 'package:sgfparser/utils/pair.dart' show Pair;

enum ModificationType { add, remove, none }

/// Class representing a stone.
/// Essentially a modulo counter. Not implemented based on an enum to facilitate
/// exotic variations like three-color go.
class StoneColor {
  late int stoneColor;
  late int maxColor;

  StoneColor(this.stoneColor, this.maxColor);
  StoneColor.empty() {
    stoneColor = 0;
    maxColor = 2;
  }
  StoneColor.fromCode(String code) {
    maxColor = 2;
    switch (code.substring(code.length - 1)) {
      case 'B':
        stoneColor = 1;
        break;
      case 'W':
        stoneColor = 2;
        break;
    }
  }
  StoneColor.copy(StoneColor x) {
    stoneColor = x.stoneColor;
    maxColor = x.maxColor;
  }

  @override
  bool operator ==(other) {
    if (other is! StoneColor) {
      return false;
    }

    return stoneColor == other.stoneColor;
  }

  /// Returns a stone for the next player.
  StoneColor next({int skip = 0}) {
    return StoneColor(
        ((stoneColor + skip + 1) % (maxColor + 1)) == 0
            ? 1
            : ((stoneColor + skip + 1) % (maxColor + 1)),
        maxColor);
  }

  /// Returns a stone for the previous player.
  StoneColor previous({int skip = 0}) {
    return StoneColor(
        (stoneColor - skip - 1) % (maxColor + 1) == 0
            ? maxColor
            : (stoneColor - skip - 1) % (maxColor + 1),
        maxColor);
  }
}

/// Class representing a stone status change descriptor.
/// Used to construct a move history.
class StoneStatus {
  StoneColor color;
  Pair? coord;
  ModificationType change;

  StoneStatus(this.color, this.coord, this.change);
}

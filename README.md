# sgfparser

An SGF parser and utility library for Dart developers.


## Usage

Basic example on how to obtain an SGF tree with metadata:

```dart
import 'package:sgfparser/sgf/sgfParser.dart';

main() {
  String sgfFile = File(<filename>).readAsStringSync();
  SGFParser parser = SGFParser.fromFile(sgfFile);
  parser.parseBuffer(); // to get an SGF tree representation at parser.games[].
  parser.parseTree(); // to get metadata.

  [...]

  String newSgf = SGFWriter().writeSgf(parser.games[0]);
}
```

Another on how to obtain a complete representation of a match parsed from the SGF (with board
descriptors etc.):

```dart
import 'package:sgfparser/sgf/sgfParser.dart';
import 'package:sgfparser/goban/match.dart';
import 'package:sgfparser/goban/gameState.dart';
import 'package:sgfparser/goban/boardTreeIterator.dart';
import 'package:sgfparser/goban/boardState.dart';
import 'package:sgfparser/goban/board.dart';
import 'package:sgfparser/goban/stone.dart';
import 'package:sgfparser/utils/pair.dart';

main() {
  Match newMatch = SGFParser.feedSgfToGame(kgsGameSgf);
  GameState gameDescriptor = newMatch.gameState;
  BoardTreeIterator boardIterator = gameDescriptor.positions.iterator;
  BoardState currentSituation = boardIterator.current;
  Board currentBoard = currentSituation.board;
  StoneColor tengen = currentBoard.getAt(Pair(10, 10));
}
```

## TODO
* Conversion from a board tree to SGF tree (required when writing an SGF in editors).
* Actual proper documentation and examples.

## Features and bugs

Feel free to use the issue tracker in this repository, all contributions and requests are welcome.
